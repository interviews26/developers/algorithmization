# Algorithmization Tasks
The following list represents basic algorithmic tasks. For each task provide a solution together with the automated tests verifying the correctness of the implementation.

1. Write a program that will take as an input a path to the file system and print all the files in the folder and its subfolders from the given path that has the suffix ".java".
2. Given a string and a string dictionary, find the longest string in the dictionary that can be formed by deleting some characters of the given string. If there is more than one possible result, return the longest word with the smallest lexicographical order. If there is no possible result, return the empty string.

The following example should clarify how it should work. In your case, create universal function(s) that will work with other input data as well.
```
Input: d = {"ale", "apple", "monkey", "plea"};
       S = "abpcplea"
Output: "apple"
```
3. Given a sorted array, the task is to remove the duplicate elements from the array. You cannot use any advanced data structures that automatically removes duplicates! Note the solution computational complexity.

The following example should clarify how it should work. In your case, create universal function(s) that will work with other input data as well.
```
Input: {1, 2, 2, 3, 4, 4, 4, 5, 5};
Output: {1, 2, 3, 4, 5};
```
4. Develop a function that will check if two input files are the same. Comment on all the things you have taken into account.
5. Given a string **S**, find the length of the longest substring without repeating characters.

The following example should clarify how it should work. In your case, create universal function(s) that will work with other input data as well.
```
Input: S = "geeksforgeeks"
Output: 7
Explanation: Longest substring is "eksforg".
```
6. Given a string of lowercase alphabets, count all possible substrings (not necessarily distinct) that have exactly k distinct characters.

The following examples should clarify how it should work. In your case, create universal function(s) that will work with other input data as well.

Example 1:
```
Input: S = "aba", K = 2
Output: 3
Explanation: The substrings are: "ab", "ba" and "aba".
```
Example 2:
```
Input: d = {"a", "b", "c"}
       S = "abpcplea"
Output: "a"
Explanation: After deleting "b", "p" "c", "p", "l", "e", "a" S became "a" which is present in d.
```
7. Given a list of words followed by two words, the task to find the minimum distance between the given two words in the list of words.

The following example should clarify how it should work. In your case, create universal function(s) that will work with other input data as well.
```
Input: S = { "the", "quick", "brown", "fox", "quick"}
word1 = "the"
word2 = "fox"
Output: 3
Explanation: Minimum distance between the words "the" and "fox" is 3
```
8. Given a string of **S** as input. Your task is to write a program to remove or delete the minimum number of characters from the string so that the resultant string is a palindrome.
Note: The order of characters in the string should be maintained.

The following example should clarify how it should work. In your case, create universal function(s) that will work with other input data as well.
```
Input: S = "aebcbda"
Output: 2
Explanation: Remove characters 'e' and 'd'.
```
9. Given a string **S** consisting of opening and closing parenthesis '(' and ')'. Find length of the longest valid parenthesis substring.

A parenthesis string is valid if:
  - For every opening parenthesis, there is a closing parenthesis.
  - Opening parenthesis must be closed in the correct order.

The following examples should clarify how it should work. In your case, create universal function(s) that will work with other input data as well.
Example 1:
```
Input: S = ((()
Output: 2
Explaination: The longest valid parenthesis substring is "()".
```
Example 2:
```
Input: S = )()())
Output: 4
Explaination: The longest valid parenthesis substring is "()()".
```
10. Given a binary tree, find its maximum depth.

The maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node. *Note*: A leaf is a node with no children.

The following example should clarify how it should work. In your case, create universal function(s) that will work with other input data as well.
```
Input: Given binary tree[3,9,20,null,null,15,7],
Output: return its depth = 3.
```


